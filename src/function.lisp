;; Motivation:
;; mathematicians write 1+f, where f is a function, to mean lambda x -> 1+f(x).
;; When coding mathematical diagrams, lisp could understand something like
;; (line 0 #'cis)
;; to mean draw a line from 0 to the point on the unit circle given by calling cis on some parameter.
;; This is the lisp analogue to a concise, natural notation for the problem domain.
;; But it has an added benefit: we can build tools for drawing stills and use them for animation by passing in functions of time instead of constants.

(defpackage geodecl.function
  (:use :cl/ga :iterate)
  (:documentation "Defines a shorthand for functions of one variable. New functions can be defined by adding, multiplying, etc, functions of time.
Examples:

(+ 1 #'f) => (lambda (time) (+ 1 (f time)))
(* #'cis #'identity) => (lambda (time) (* (cis time) time))

Offers alternatives to the standard functions `funcall',  `apply', and `defun' that use this shorthand.
Implements variants of the cl/ga math operators that use this convention.

This is useful for animation. For example, the radius of a rotating circle might be drawn by

(line 0 #'cis) => (lambda (time) (line 0 (cis time)))
"))
(in-package :geodecl.function)

(named-readtables:in-readtable geodecl.readtable:syntax)


(defun functionyp (fn)
  "In case we ever decide to represent functions with objects that aren't funcallable, eg tables"
  (functionp fn))

@export
(defmethod fwrap (thing)
  "Creates a function that assigns behavior to thing.
Examples:
If thing is a constant, (fwrap thing) is the function that returns thing.
If thing is already a function, (fwrap fn) = fn "
  (assert (not (functionyp thing)))
  (lambda (&rest args)
    (declare (ignore args))
    thing))
(defmethod fwrap ((fn function)) fn)

@export
(defun fapply (fn list)
  "Applies fn on list. If fn is not a function, we consider it to be the constant function that returns fn. If some of the arguments are functions, compose instead."
  (if (some #'functionyp list)
      (lambda (&rest args)
        (apply (fwrap fn)
               (mapcar [(fapply _0 args)] list)))
      (apply (fwrap fn) list)))

@export
(defun fcall (fn &rest args)
  "Calls fn on args. If fn is not a function, we, consider it to be the constant function that returns fn. If some of the arguments are functions, compose instead.
Example:

(fcall + 1 #'identity) => (lambda (x) (+ 1 x))"
  (fapply fn args))

@export
(defmacro defun-fcall (name arglist &body body)
  "Like defun, but uses fcall."
  (alexandria:with-gensyms (args)
    `(defun ,name (&rest ,args)
       (fapply (lambda ,arglist ,@body)
               ,args))))
;; fix slime arglists?
;; docstring?


;; CL (wisely?) locks the CL package. But we can still redefine mathematical operators to use fcall, thanks to cl/ga
;; nullary
(defmethod nullary-+ (f) 0)
(defmethod nullary-* (f) 1)
;; unary methods
(defmethod unary-+ (f) (fcall #'cl:+ f))
(defmethod unary-- (f) (fcall #'cl:- f))
(defmethod unary-* (f) (fcall #'cl:* f))
(defmethod unary-/ (f) (fcall #'cl:/ f))
;; binary methods
(defmethod binary-+ (f g) (fcall #'cl:+ f g))
(defmethod binary-- (f g) (fcall #'cl:- f g))
(defmethod binary-* (f g) (fcall #'cl:* f g))
(defmethod binary-/ (f g) (fcall #'cl:/ f g))
;; other good things
(defmacro unary-fcall/ga (&body fns)
  `(progn
     ,@(iter (for fn in fns)
         (collect
             `(defmethod ,fn (f)
                (fcall #',fn f))))))
(unary-fcall/ga exp cis cos sin tan cosh tanh sinh 1+ 1- sqrt realpart imagpart)

(defmethod log (number &optional (base (exp 1)))
  (fcall #'log number base))
(defmethod expt (base expt)
  (fcall #'expt base expt))
(defmethod complex (realpart &optional (imagpart 0))
  (fcall #'cl:complex realpart imagpart))
