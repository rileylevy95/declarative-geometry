;; This file goes from picture language -> svg
(defpackage geodecl.svg
  (:use :cl/ga :iterate :geodecl.function :geodecl.picture :anaphora :geodecl.utils)
  (:import-from :repl-utilities :deflex)
  (:shadowing-import-from :geodecl.picture :class)
  (:shadow :time)
  (:export :time))

(in-package :geodecl.svg)
(named-readtables:in-readtable geodecl.readtable:syntax)

(deflex time #'identity)

@export
(defmacro with-accessors-abbrev (accessors obj &body body)
  "with-slots for when the slot name is the variable name"
  `(with-accessors
         ,(iter (for acc in accessors)
           (collect `(,acc ,acc)))
       ,obj
     ,@body))

@export
(defmethod proc-args ((node node))
  "Turns the info in node into a plist, ready for use with `cl-svg'."
  (with-accessors-abbrev (id class visibility) node
    (remove nil
            (append (if id `(:id ,id))
                    (list :visibility visibility)
                    (if class
                        (list :class (format nil "~{~a~^ ~}" class)))))))

(defmethod proc-args ((circle circle))
  (with-accessors-abbrev (center radius) circle
    (list* :circle
           :cx (float (realpart center))
           :cy (float (imagpart center))
           :r (float radius)
           (call-next-method))))

(defmethod proc-args ((line line))
  (with-accessors-abbrev  (start end) line
    (list* :line
           :x1 (float (realpart start))
           :y1 (float (imagpart start))
           :x2 (float (realpart end))
           :y2 (float (imagpart end))
           (call-next-method))))

(defmethod proc-args ((path path))
  (list* :path
         :d (d path)
         (call-next-method)))

(defun points->str (seq)
  "Print out a list of points as x1,y1 x2,y2 ..."
  (format nil "~{~{~$~^,~}~^ ~}"
          (map 'list (juxt #'realpart #'imagpart) seq)))

(defmethod proc-args ((poly polyline))
  (with-accessors-abbrev (points) poly
    (list* :polyline :points (points->str points)
           (call-next-method))))

(defmethod proc-args ((polygon polygon))
  (list* :polygon :points (points->str (points polygon))
         (call-next-method)))

(defmethod proc-args ((marker marker))
  (with-accessors-abbrev (view-box ref-pt size units orient) marker
    (list* :marker
           :view-box (format nil "~{~a~^ ~}" view-box)
           :ref-x (realpart ref-pt)
           :ref-y (imagpart ref-pt)
           :marker-width (realpart size)
           :marker-height (imagpart size)
           :marker-units (or units "strokeWidth")
           :orient orient
           (call-next-method))))

(defun convert-svg-elt (node)
  "This makes an svg element from a node."
  ;;  Unfortunately, `svg:draw' is awkward to use at run-time, but `make-svg-element' is not external. It probably should be.
  (svg::make-svg-element (car (proc-args node))
                         (cdr (proc-args node))))

@export
(defvar *canvas* nil "Stores the current cl-svg canvas")

(defun drawn-children (node)
  "Draws a node and children. Returns the resulting svg code."
  (let ((elt (convert-svg-elt node)))
    (let ((*canvas* elt))
      (map nil #'draw (children node)))
    elt))

@export
(defmethod draw ((node node) &optional (canvas *canvas*))
  "Drawn `node' onto `canvas'"
  (svg:add-element canvas (drawn-children node)))

(defmethod anchor->number ((anchor number)) anchor)
(defvar *line->number-function*
  (lambda (anchor)
    (declare (line anchor))
    (avg (start anchor) (end anchor))))
(defmethod anchor->number ((anchor line))
  (funcall *line->number-function* anchor))
;; there is no contextual clos dispatch. but this functionality is a subset of contextl

(defmethod draw ((label label) &optional (canvas *canvas*))
  (let ((pos (+ (anchor->number (anchor label)) (dz label))))
    (svg:add-element
     canvas
     (format nil "<g transform=\"translate(~$,~$)\"><text>~a</text></g>"
             (realpart pos)
             (imagpart pos)
             (text label)))))

(defmethod draw ((marker marker) &optional (canvas *canvas*))
  (svg::add-defs-element canvas (drawn-children marker)))

@export
(defparameter *marker-sty*
  "
.end-arrow{
marker-end: url(#arrow);
}
.start-arrow{
marker-start: url(#arrow);
}
.mid-arrow{
marker-mid: url(#arrow);
}
")

(defparameter *mark-arrow-defaults*
  `((:view-box 0 0 10 10)
    (:ref-pt . #C(10 5))
    (:size . #C(10 10))
    (:orient . "auto-start-reverse")
    (:children . ,(list (path "M 0 0 L 10 5 L 0 10 z")))))

@export
(defun mark-arrow (id &rest keys-plist &key &allow-other-keys)
  (apply #'marker
         :id id
         ;; defaults!!
         (alexandria:alist-plist
          (append (alexandria:plist-alist keys-plist)
                  *mark-arrow-defaults*))))
@export
(defvar *css* *marker-sty* "The default inline css")
@export
(defvar *default-stylesheet* nil "The default external css sheet")

@export
(defmacro with-svg ((&rest svg-args &key (stylesheet *default-stylesheet*) &allow-other-keys) &body body)
  "Sets up an svg-drawing environment."
  `(lambda (dir time)
     (let ((file (merge-pathnames (make-pathname :name (format nil "~4$" time) :type "svg")
                                  dir))
           *nodes*)
       (svg:with-svg-to-file
           (*canvas* 'svg:svg-1.2-toplevel ,@svg-args)
         (file :if-does-not-exist :create)
         ;; the following belongs in css but idk if browsers support that??
         ,(aif stylesheet `(svg:add-stylesheet *canvas* ,it))
         (mark-arrow "arrow")
         (mark-arrow "axis-arrow")
         (if *css* (svg:style *canvas* *css*))
         ,@body
         (mapcar [(let (*nodes*)
                    ;;drawing should not affect *nodes* bindings.
                    (draw (fcall _0 time)))]
                    *nodes*)))))

@export
(defvar *frames* 0)
@export
(defvar *framerate* 1)

@export
(defun spit-svg (fun dir &key (dur 1) (framerate *framerate* frameratep)
                           (name "output.svg")
                           (frames (if frameratep
                                       (* framerate dur)
                                       *frames*)))
  "Writes the data created by a with-svg to files in directory `dir'. Each file is a frame, named for the time that frame is displayed."
  (declare (pathname dir))
  (assert (uiop:directory-pathname-p dir))
  (if (probe-file dir)
      (restart-case (error "directory ~a already exists" dir)
        (overwrite ()
          (uiop:delete-directory-tree dir
                                      :validate t ;; i don't get it
                                      :if-does-not-exist :ignore))
        (skip ()
          (return-from spit-svg nil))))
  (let ((dir (merge-pathnames #P"svg/" dir)))
    (assert (uiop:directory-pathname-p dir))
    (ensure-directories-exist dir)
    (map nil
         [(funcall fun dir _0)]
         (if (zerop frames) (list 0)      ;division by 0 otherwise
             (iter (for i from 0 to frames)
               (collect (* dur i (/ frames)))))))
  (uiop:with-current-directory (dir)
    (uiop:run-program
     (format nil
             (if (> frames 0)
                 "../../svganimator/svganimator.py -b -s .1 ~a $(ls svg/* | sort -V)"
                 "cp $(ls svg/*) ~a")
             name))))
