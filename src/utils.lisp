(defpackage geodecl.utils
  (:use :cl/ga :iterate :anaphora)
  (:import-from :repl-utilities :deflex)
  (:shadow :time))


(in-package :geodecl.utils)
(named-readtables:in-readtable geodecl.readtable:syntax)

@export
(defun slurp (path)
  "Reads in a file and returns its contents as a string."
  (with-open-file (stream path)
    (let ((data (make-string (file-length stream))))
      (read-sequence data stream)
      data)))
;;  see https://www.rosettacode.org/wiki/Read_entire_file#Common_Lisp

@export
(defun juxt (&rest fns)
  "Cartesian product of functions, ie (juxt f1 f2 f3) creates a function that,
when called on x, returns (list (funcall f1 x) (funcall f2 x) (funcall f3 x)) "
  (lambda (&rest args)
    (mapcar [(apply _0 args)] fns)))

@export
(defun avg (&rest pts)
  "Averages the elements of `pts'."
  (/ (apply #'+ pts) (length pts)))
