(ql:quickload 'geodecl)
(defpackage example
  (:use :cl/ga :iterate :geodecl.utils :geodecl.picture :geodecl.svg)
  (:shadowing-import-from :geodecl.svg :time)
  (:shadowing-import-from :geodecl.picture :class)
  (:import-from :repl-utilities :deflex))
(in-package :example)

(named-readtables:in-readtable geodecl.readtable:syntax)
(proclaim '(optimize (safety 3) (debug 3) (speed 0)))

(setf *css* (concatenate 'string geodecl.svg:*marker-sty* (slurp "style.css")))

;; --------------------------------------
;; euler's theorem
(defun factorial (n &optional (prod 1))
  "naive factorial algorithm"
  (if (< n 2) prod
      (factorial (1- n) (* n prod))))

(defun term-n (z n)
  "The n-th term in the taylor series of e^iz"
  (/ (expt (* i z) n)
     (factorial n)))

(defun sum-n (z n)
  "The sum of the first n terms in the taylor series of e^iz"
  (if (< n 0) 0
      (iter (for k from 0 below n)
        (summing (term-n z k)))))

(let ((terms 80))
  (spit-svg
   (with-svg (:view-box "-40 -40 80 80")
     (circle 0 1 :class '("axis"))
     (iter (for n from 0 to terms)
       (line (sum-n time (1- n))
             (sum-n time n))))
   #P"examples/taylor/" :dur (* 4 tau) :framerate 30))

;; --------------------------------------
;; parametric test -- draw a spiral
(spit-svg
 (with-svg (:view-box "-4 -4 8 8")
   (parametric (* 1/8 #'identity (cis #'identity)) :dur 32 :slices 300))
 #P"examples/spiral/")


;; --------------------------------------
;; derivative of circle
(spit-svg
 (with-svg (:view-box "-2 -2 4 4")
   (circle 0 1)
   (label (line 0 #'cis :class '("end-arrow")) "z")
   (let ((*default-interp-tdur* (/ tau 4)))
     (if (< time *default-interp-tdur*)
         (line #'cis (* #C(1 1) #'cis) :class '("end-arrow" "axis")))
     (label (line (interp #'cis 0) (* (interp #C(1 1) i) #'cis)
                  :class '("end-arrow")) "z'")
     (if (> time (+ *default-interp-tdur* .5))
         (label (line (* i #'cis) (* #C(-1 1) #'cis) :class '("end-arrow"))
                "z''"))))
 #P"examples/circle-derivatives/" :dur  tau :frames 200)

;;--------------------------------------
;; cycloid
(defun cycloid (time)
  (+ time
     (* i (cis time))))

(spit-svg
 (with-svg (:view-box "-1 -2 28 4")
   (circle (cycloid time) .1 :class '("dot"))
   (circle time 1)
   (parametric #'cycloid :dur time))
 #P"examples/cycloid/" :dur (* 4 tau) :frames 100)

;; spirograph

(defun spirograph (speed)
  (flet ((inner (time)
           (* 2 (cis time)))
         (outer (time)
           (* (cis (* speed time)))))
    (parametric (+ #'inner #'outer) :dur (* 8 tau) :slices 2000)))

(spit-svg
 (with-svg (:view-box "-5 -5 10 10")
   (spirograph time))
 #P"examples/spiral2/" :dur (* tau) :frames 2000)

;; quadratic spline test
